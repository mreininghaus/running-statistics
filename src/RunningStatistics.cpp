/*
    librstat
    Copyright (C) 2021 Maximilian Reininghaus

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstddef>

#include <gsl/gsl_rstat.h>

#include <RunningStatistics.hpp>

RunningStatistics::RunningStatistics()
{
    rstat.median_workspace_p = &median_workspace;
    gsl_rstat_reset(&rstat);
}

RunningStatistics::RunningStatistics(RunningStatistics const& other)
    : rstat { other.rstat }
    , median_workspace { other.median_workspace }
{
    rstat.median_workspace_p = &median_workspace;
}

int RunningStatistics::reset()
{
    return gsl_rstat_reset(&rstat);
}

void RunningStatistics::add(double val)
{
    gsl_rstat_add(val, &rstat);
}

int RunningStatistics::n() const
{
    return gsl_rstat_n(&rstat);
}

double RunningStatistics::min() const
{
    return gsl_rstat_min(&rstat);
}

double RunningStatistics::max() const
{
    return gsl_rstat_max(&rstat);
}

double RunningStatistics::mean() const
{
    return gsl_rstat_mean(&rstat);
}

double RunningStatistics::variance() const
{
    return gsl_rstat_variance(&rstat);
}

double RunningStatistics::std_dev() const
{
    return gsl_rstat_sd(&rstat);
}

double RunningStatistics::kurtosis() const
{
    return gsl_rstat_kurtosis(&rstat);
}

double RunningStatistics::skew() const
{
    return gsl_rstat_skew(&rstat);
}

double RunningStatistics::median()
{
    return gsl_rstat_median(&rstat);
}

double RunningStatistics::std_dev_mean() const
{
    return gsl_rstat_sd_mean(&rstat);
}

double RunningStatistics::rms() const
{
    return gsl_rstat_rms(&rstat);
}
