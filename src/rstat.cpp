#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <string>

#include "RunningStatistics.hpp"

//#define DOCOPT_HEADER_ONLY
#include <docopt.h>

static const char USAGE[] =
R"(Usage: rstat [options] FILE

Options:
  -m, --mean            print mean
  -o, --stddev-mean     print standard deviation of mean
  -s, --stddev          print standard deviation
  -v, --variance        print variance
  -n, --numsamples      print number of samples
  --min                 print minimum
  --max                 print maximum
  --precision=<digits>  number of digits to print [default: 5]
)";

int main(int argc, char** argv) {
  std::map<std::string, docopt::value> args = docopt::docopt(USAGE,
    { argv + 1, argv + argc }, true, "rstat 0.1");

  std::string const filename = args["FILE"].asString() == "-"? "/dev/stdin" : args["FILE"].asString();
  std::ifstream input(filename);

  auto digits = args["--precision"].asLong();

  RunningStatistics statistics;

  for (double v; input >> v;)
  {
    statistics.add(v);
  }

  if (auto a = args["--numsamples"]; a && a.asBool())
    std::cout << "samples: " << std::setw(digits) << statistics.n() << std::endl;

  if (auto a = args["--mean"]; a && a.asBool())
    std::cout << "mean: " << std::setw(digits) << statistics.mean() << std::endl;
  
  if (auto a = args["--stddev-mean"]; a && a.asBool())
    std::cout << "std. dev. mean: " << std::setw(digits) << statistics.std_dev_mean() << std::endl;

  if (auto a = args["--stddev"]; a && a.asBool())
    std::cout << "std. dev.: " << std::setw(digits) << statistics.std_dev() << std::endl;

  if (auto a = args["--variance"]; a && a.asBool())
    std::cout << "variance: " << std::setw(digits) << statistics.variance() << std::endl;

  if (auto a = args["--min"]; a && a.asBool())
    std::cout << "minimum: " << std::setw(digits) << statistics.min() << std::endl;

  if (auto a = args["--max"]; a && a.asBool())
    std::cout << "maximum: " << std::setw(digits) << statistics.max() << std::endl;

  return EXIT_SUCCESS;
}
