/*
    librstat
    Copyright (C) 2021 Maximilian Reininghaus

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cstddef>

#include <gsl/gsl_rstat.h>

class RunningStatistics {

    gsl_rstat_workspace rstat;
    gsl_rstat_quantile_workspace median_workspace;

public:
    RunningStatistics();

    RunningStatistics(RunningStatistics const& other);
    int reset();

    void add(double);

    int n() const;

    double min() const;

    double max() const;

    double mean() const;

    double variance() const;

    double std_dev() const;

    double kurtosis() const;

    double skew() const;

    double median();

    double std_dev_mean() const;

    double rms() const;
};
